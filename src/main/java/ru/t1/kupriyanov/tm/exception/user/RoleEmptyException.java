package ru.t1.kupriyanov.tm.exception.user;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! Role is empty!");
    }

}
