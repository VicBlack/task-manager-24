package ru.t1.kupriyanov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
