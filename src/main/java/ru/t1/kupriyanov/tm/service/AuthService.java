package ru.t1.kupriyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.api.service.IAuthService;
import ru.t1.kupriyanov.tm.api.service.IUserService;
import ru.t1.kupriyanov.tm.enumerated.Role;
import ru.t1.kupriyanov.tm.exception.user.LoginEmptyException;
import ru.t1.kupriyanov.tm.exception.user.AccessDeniedException;
import ru.t1.kupriyanov.tm.exception.user.PasswordEmptyException;
import ru.t1.kupriyanov.tm.exception.user.PermissionException;
import ru.t1.kupriyanov.tm.model.User;
import ru.t1.kupriyanov.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService extends AbstractService<User, IUserService> implements IAuthService {

    //private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        super(userService);
    }

    @NotNull
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return repository.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = repository.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final boolean locked = user.getLocked() == null || user.getLocked();
        if (locked) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = repository.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @Nullable final User user = getUser();
        @Nullable final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
